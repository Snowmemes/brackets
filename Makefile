
CXX = g++
CXX_FLAGS = -Wall -Wextra -pedantic -fsanitize=address -g -Og

exe: main.cpp
	$(CXX) $(CXX_FLAGS) main.cpp -o exe

.PHONY: run
run: exe
	@./exe

