
#include <bits/stdc++.h>

using namespace std;

int main() {
	int o[ 1000000 ] = { 0 }, c[ 1000000 ] = { 0 };

	stack<int> s;
	int pos = 0;

	while ( !cin.eof() ) {
		char ch = 'a';
		cin >> ch;

		if ( ch == '(' ) {
			s.push( pos );
			c[ pos ] = -1;
		} else if ( ch == ')' && s.empty() ) {
			o[ pos ] = -1;
			c[ pos ] = -1;
		} else if ( ch == ')' && !s.empty() ) {
			o[ pos ] = s.top();

			if ( o[ pos ] > 0 && c[ o[ pos ] - 1 ] > -1 ) {
				c[ pos ] = c[ o[ pos ] - 1 ];
			} else {
				c[ pos ] = o[ pos ];
			}

			s.pop();
		}

		pos++;
	}

	/*
	for ( int i = 0; i < pos; i++ ) {
		cout << o[ i ] << " ";
	}
	cout << endl;
	for ( int i = 0; i < pos; i++ ) {
		cout << c[ i ] << " ";
	}
	cout << endl;
	*/

	int m = -1;
	int cnt = 0;
	for ( int i = 0; i < pos - 1; i++ ) {
		if ( c[ i ] > -1 ) {
			//cout << "  : " << i << endl;
			if ( i - c[ i ] == m - 1 ) {
				cnt++;
			} else {
				cnt = 1;
			}

			m = max( m, i - c[ i ] + 1);
		}
	}

	if ( cnt == 0 ) {
		cout << "0 1" << endl;
	} else {
		cout << m << " " << cnt << endl;
	}

	return 0;
}
